export default {
  namespaced: true,
  state: {
    items: []
  },
  getters: {
    items(state) {
      return state.items;
    },
    bookInFavorites: (state) => (id) => {
      
      if (!id) return false;
      
      for (let i = 0; i < state.items.length; i++) {
        if (state.items[i].id === id) return true;
      }
  
      return false;
    },
  },
  mutations: {
    addItem(state, payload) {
      state.items.push(payload);
    }
  },
  actions: {
    addItem(context, payload) {
      
      if (!payload) return;
      
      context.commit('addItem', payload);
    }
  }
};
