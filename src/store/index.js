import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import search from './modules/search';
import book from './modules/book';
import favorites from './modules/favorites';

export const store = new Vuex.Store({
  modules: {
    search,
    book,
    favorites
  },
  
  state: {
    showIntro: true
  },
  
  getters: {
    showIntro(state) {
      return state.showIntro;
    }
  },
  
  mutations: {
    setViewIntroStatus(state, payload) {
      state.showIntro = payload;
    }
  },
  
  actions: {
    setViewIntroStatus(context, payload) {
      context.commit('setViewIntroStatus', payload);
    }
  }
});
