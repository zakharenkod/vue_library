import axios from 'axios';

const apiKey = 'AIzaSyCS_lWYwfbLFAMHN8ah3qXU9nzbQRW583o';

export default {
  namespaced: true,
  state: {
    searchInputText: '',
    searchResultItems: [],
    showAutoComplete: false,
    searchAutoCompleteCount: 5,
    showSearchResults: false,
    searchFormSubmitted: false,
    resultsItems: []
  },
  
  getters: {
    searchInputText(state) {
      return state.searchInputText;
    },
    searchResultItems(state) {
      return state.searchResultItems;
    },
    showAutoComplete(state) {
      return state.showAutoComplete;
    },
    searchAutoCompleteCount(state) {
      return state.searchAutoCompleteCount;
    },
    showSearchResults(state) {
      return state.showSearchResults;
    },
    searchFormSubmitted(state) {
      return state.searchFormSubmitted;
    },
    searchAutoCompleteItems(state, getters) {
      return getters.searchResultItems.slice(0, getters.searchAutoCompleteCount);
    },
    showIntro(state, getters) {
      return !getters.showSelectedBook;
    },
    resultsItems(state) {
      return state.resultsItems;
    }
  },
  
  mutations: {
    updateSearchValue(state, value) {
      state.searchInputText = value;
    },
    clearSearchValue(state) {
      state.searchInputText = '';
    },
    addItems(state, arr) {
      state.searchResultItems = arr;
    },
    clearItems(state) {
      state.searchResultItems = [];
    },
    setSubmittedStatus(state, status) {
      state.searchFormSubmitted = status;
    },
    setAutocompleteViewStatus(state, status) {
      state.showAutoComplete = status;
    },
    setSearchResultsViewStatus(state, status) {
      state.showSearchResults = status;
    },
    addResults(state, arr) {
      state.resultsItems = arr;
    },
    clearResults(state) {
      state.resultsItems = [];
    }
  },
  
  actions: {
    getSearchData(context, searchString) {
      axios.get(`https://www.googleapis.com/books/v1/volumes?q=${searchString}&key=${apiKey}`)
        .then(response => {
  
          if (!response.data.items) return;
  
          context.commit('addItems', response.data.items);
  
          if (!context.getters.searchFormSubmitted) {
            context.commit('setAutocompleteViewStatus', true);
          }
  
        })
        .catch(error => console.log(`Error loading search results: \n` + error));
    },
    updateSearchValue(context, value) {
      context.commit('updateSearchValue', value);
      context.commit('setSubmittedStatus', false);
  
      if (value) {
        context.dispatch('getSearchData', value);
      } else {
        context.commit('clearItems');
        context.commit('setAutocompleteViewStatus', false);
      }
    },
    clearSearchForm(context) {
      context.commit('clearSearchValue');
      context.commit('clearItems');
      context.commit('setAutocompleteViewStatus', false);
    },
    submitForm(context) {
      
      if (context.getters.searchInputText) {
        context.dispatch('getSearchData', context.getters.searchInputText);
        context.commit('setSearchResultsViewStatus', true);
        context.commit('clearResults');
        context.commit('addResults', context.getters.searchResultItems.slice());
      }
  
      context.commit('setSubmittedStatus', true);
      context.commit('setAutocompleteViewStatus', false);
    }
  }
};
