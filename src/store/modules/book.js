export default {
  namespaced: true,
  state: {
    id: '',
    img: '',
    title: '',
    authors: [],
    publishedDate: '',
    description: '',
    averageRating: 0,
    showBookInfo: false,
    inFavorites: false
  },
  
  getters: {
    id(state) {
      return state.id;
    },
    img(state) {
      return state.img;
    },
    title(state) {
      return state.title;
    },
    authors(state) {
      return state.authors;
    },
    publishedDate(state) {
      return state.publishedDate;
    },
    description(state) {
      return state.description;
    },
    averageRating(state) {
      return state.averageRating;
    },
    showBookInfo(state) {
      return state.showBookInfo;
    },
    inFavorites(state) {
      return state.inFavorites;
    }
  },
  
  mutations: {
    clearBookData(state) {
      state.id = null;
      state.img = '';
      state.title = '';
      state.authors = [];
      state.publishedDate = '';
      state.description = '';
      state.averageRating = 0;
      state.inFavorites = false;
    },
    setBookData(state, data) {
      state.id = data.id;
      state.title = data.volumeInfo.title || '';
      state.publishedDate = data.volumeInfo.publishedDate || '';
      state.description = data.volumeInfo.description || '';
      state.averageRating = data.volumeInfo.averageRating || 0;
      
      if (data.volumeInfo.authors && data.volumeInfo.authors.length > 0) {
        state.authors = data.volumeInfo.authors.slice();
      } else {
        state.authors = [];
      }

      if (data.volumeInfo.imageLinks && data.volumeInfo.imageLinks.thumbnail) {
        state.img = data.volumeInfo.imageLinks.thumbnail;
      } else {
        state.img = '';
      }
      
    },
    setViewBookStatus(state, payload) {
      state.showBookInfo = payload;
    },
    setFavoritesStatus(state, payload) {
      state.inFavorites = payload;
    }
  },
  
  actions: {
    setViewBookStatus(context, payload) {
      context.commit('setViewBookStatus', payload);
    },
    setFavoritesStatus(context, payload) {
      context.commit('setFavoritesStatus', payload);
    },
    showBookData(context, payload) {
      context.commit('clearBookData');
      context.commit('setFavoritesStatus', payload.inFavorites);
      context.commit('setBookData', payload.bookData);
      context.commit('setViewBookStatus', true);
    }
  }
};
