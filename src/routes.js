import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import AppMain from './components/Main';
import AppFavorites from './components/Favorites'

const routes = [
  {
    name: 'main',
    path: '',
    component: AppMain
  }, {
    name: 'favorites',
    path: '/favorites',
    component: AppFavorites
  }, {
    path: '*',
    redirect: {name: 'main'}
  }
];

export const router = new VueRouter({
  routes,
  mode: 'history'
});
